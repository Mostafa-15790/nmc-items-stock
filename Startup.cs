﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NMCStock.Startup))]
namespace NMCStock
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
