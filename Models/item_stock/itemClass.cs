﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NMCStock.Models.item_stock
{
    public class itemClass
    {
        public int item_id { get; set; }
        public string item_name { get; set; }
        public int item_quantity { get; set; }
    }
}